/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/


#ifndef ATHONNXCOMPSSDICT_H
#define ATHONNXCOMPSSDICT_H

// Includes for the dictionary generation:

#include "AthOnnxComps/OnnxRuntimeInferenceTool.h"
#include "AthOnnxComps/OnnxRuntimeSessionToolCPU.h"
#include "AthOnnxComps/OnnxRuntimeSessionToolCUDA.h"
#include "AthOnnxComps/OnnxRuntimeSvc.h"

#endif // ATHONNXCOMPSSDICT_H
