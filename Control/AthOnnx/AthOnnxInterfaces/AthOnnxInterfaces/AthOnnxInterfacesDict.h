/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/


#ifndef ATHONNXINTERFACESDICT_H
#define ATHONNXINTERFACESDICT_H

// Includes for the dictionary generation:

#include "AthOnnxInterfaces/IOnnxRuntimeSvc.h"
#include "AthOnnxInterfaces/IOnnxRuntimeSessionTool.h"
#include "AthOnnxInterfaces/IOnnxRuntimeInferenceTool.h"

#endif // ATHONNXINTERFACESDICT_H
